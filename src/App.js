import React, {useState} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

/*components*/
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
// import CourseCard from './components/CourseCard';
// import Welcome from './components/Welcome'
import Courses from './pages/Courses';
// import Counter from './components/Counter'
import Register from './pages/Register';
import LogIn from './pages/LogIn';
import Container from './components/Container'

export default function App(){

	const [user, setUser] = useState(null);

	return(
		// <Fragment>
		//   <AppNavbar/>
		//   <Home/>
		//   <CourseCard/>
		//  <Welcome name="John"/>
		//   <Welcome name="Lawrence"/>
		//   <Courses/>
		//   <Counter/>
		//  <Register/>
		//  <LogIn/>
		// </Fragment>

	<BrowserRouter>
		<AppNavbar user={user} />				{/*//first user is props from APpnavbar*/}
		<Switch>
			<Route exact path ="/" component={Home} />
			<Route exact path ="/courses" component={Courses} />
			<Route exact path ="/register" component={Register} />
			<Route exact path ="/login" component={LogIn} />
			<Route component={Container}/>
		</Switch>
	</BrowserRouter>
	)
}
import React from 'react';
import {Link} from 'react-router-dom';

import {
	Container,
	Row,
	Col,
	Jumbotron,
	Button
} from 'react-bootstrap';

export default function container(){

	return(
		<Container fluid>
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-3">
					  <h1>404 Error</h1>
					  <p>The page you are looking for cannot be found</p>
					  <a href="/" as={Link}>Back Home</a>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}
